import Vue from 'vue'

let privates = {}

export const state = () => ({
  doctors: [
    {
      id: 1,
      name: 'Joe Ballack',
      email: 'joe@gmail.com',
      description: 'about Joe',
      specialty: 'Cardiologist',
      likes: 3,
      liked: false,
      comments: [
        {
          name: 'Mahya amiri',
          text: 'this a comment from mahya'
        },
        {
          name: 'sara',
          text: 'this a comment'
        }
      ]
    },
    {
      id: 2,
      name: 'Jack pendelton',
      email: 'jackiii@gmail.com',
      description: 'about jack',
      specialty: 'Psychiatrist',
      likes: 12,
      liked: true,
      comments: [
        {
          name: 'Mahya amiri',
          text: 'this a comment from mahya'
        }
      ]
    }
  ],
  doctor: {},
  sortType: 'up'
})

export const getters = {
  doctors(state) {
    return state.doctors
  },
  doctor(state) {
    return state.doctor
  },
  sortType(state) {
    return state.sortType
  }
}

export const mutations = {
  setSelectedDoctor(state, doctor) {
    state.doctor = Object.assign({}, doctor)
  },
  sortAscDesc(state) {
    state.doctors.sort((a, b) => {
      return state.sortType === 'down' ? a.likes - b.likes : b.likes - a.likes;
    });
    state.sortType = (state.sortType === 'down') ? 'up' : 'down'
  },
  submitComment(state, payload) {
    let index = state.doctors.findIndex(x => x.id === payload.id)
    if( index !== -1) {
        state.doctors[index].comments.push({
          name: 'My profile Name',
          text: payload.text
        })
    }
  },
  toggleLikeDislike(state, doctorId) {
    let index = state.doctors.findIndex(x => x.id === doctorId)
    if( index !== -1) {
      state.doctors[index].liked = !state.doctors[index].liked
      state.doctors[index].likes = state.doctors[index].liked ? (state.doctors[index].likes + 1) : (state.doctors[index].likes - 1)
    }
  },
  addNewDoctor(state, doctor) {
    state.doctors.push(doctor)
  }
}

export const actions = {
  setSelectedDoctor({commit}, doctor) {
    commit('setSelectedDoctor', doctor)
  },
  sortAscDesc({commit}) {
    commit('sortAscDesc')
  },
  submitComment({commit}, payload) {
    commit('submitComment', payload)
  },
  toggleLikeDislike({commit}, doctorId) {
    commit('toggleLikeDislike', doctorId)
  },
  addNewDoctor({commit}, doctor) {
    commit('addNewDoctor', doctor)
  }
}
